import React from 'react';

import Stack from '@mui/material/Stack';
import SendIcon from '@mui/icons-material/Send';
import Typography from '@mui/material/Typography';
import ActionBar from '@components/shared/ActionBar';
import DownloadIcon from '@mui/icons-material/Download';
import BalanceList from '@components/balance/BalanceList';
import RootContainer from '@components/layouts/RootContainer';
import CustomTabs from '@components/shared/custom/CustomTabs';

import { NativeAssetHashMap } from '@interfaces/balance';
import { useGetBalancesQuery } from '@redux/services/balances';

const HomePage: React.FC = () => {
  const { data: balances = [], isLoading, isError, refetch: refetchBalance } = useGetBalancesQuery();

  const nativeAssetHashmap = React.useMemo(() => (
    balances.reduce<NativeAssetHashMap>((prev, curr) => (
      curr.standard === 'native' ? { ...prev, [curr.blockchain]: curr } : prev
    ), {})
  ), [balances]);

  const sortedBalances = React.useMemo(() => balances.slice().sort((a, b) => a.standard === 'native' && b.standard !== 'native'
    ? -1
    : a.standard !== 'native' && b.standard === 'native'
      ? 1
      : a.ticker.localeCompare(b.ticker)
  ), [balances]);

  React.useEffect(() => {
    const intervalId = setInterval(() => refetchBalance(), 5000);
    return () => clearInterval(intervalId);
  }, []);

  return (
    <RootContainer>
      <Stack height="100%">
        <Stack justifyContent="space-evenly" minHeight={160}>
          <Typography variant="h5" sx={{ color: 'white', px: 3, textAlign: 'center' }}>$5000</Typography>
          <ActionBar
            actions={[{
              name: 'Send',
              icon: <SendIcon
                sx={{
                  width: theme => theme.sizing.width.avatar.action,
                  height: theme => theme.sizing.height.avatar.action
                }}
              />,
              onClick: () => {}
            }, {
              name: 'Receive',
              icon: <DownloadIcon
                sx={{
                  width: theme => theme.sizing.width.avatar.action,
                  height: theme => theme.sizing.height.avatar.action
                }}
              />,
              onClick: () => {}
            }]}
          />
        </Stack>
        <CustomTabs
          tabLabels={['Tokens', 'NFTs']}
          tabPanels={[
            <BalanceList
              key="tab-0"
              error={isError}
              loading={isLoading}
              balances={sortedBalances}
              nativeAssetHashmap={nativeAssetHashmap}
            />,
            'Under construction'
          ]}
        />
      </Stack>
    </RootContainer>
  );
};

export default HomePage;
