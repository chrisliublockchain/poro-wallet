import React from 'react';
import ReactDOM from 'react-dom/client';
import { SnackbarProvider } from 'notistack';
import { ThemeProvider } from '@mui/material';
import { Provider as ReduxProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Routes, Route, MemoryRouter } from 'react-router-dom';

import theme from '@themes/index';

import navigations from './navigation';
import { persistor, store } from './redux';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <SnackbarProvider
            maxSnack={1}
            preventDuplicate
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right'
            }}
            style={{ boxShadow: 'none' }}
          >
            <MemoryRouter
              initialIndex={0}
              initialEntries={navigations.map((nav) => nav.path)}
            >
              <Routes>
                {
                  navigations.map((nav) => (
                    <Route key={`route-${nav.path}`} path={nav.path} element={nav.component} />
                  ))
                }
              </Routes>
            </MemoryRouter>
          </SnackbarProvider>
        </ThemeProvider>
      </PersistGate>
    </ReduxProvider>
  </React.StrictMode>
);
