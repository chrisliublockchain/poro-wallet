/* eslint-disable import/prefer-default-export */
import { Client } from 'xrpl';
import BigNumber from 'bignumber.js';
import { RippleBuiltTx } from '@poro-wallet/core';

import { Balance } from '@interfaces/balance';
import { BuildTxParams } from '@interfaces/transaction';
import { BASE_UNIT } from '@poro-wallet/core/built/constants';

const buildNativeTx = async (params: BuildTxParams, node: string): Promise<RippleBuiltTx> => {
  const { from: Account, to: Destination, amount } = params;
  const client = new Client(node);
  await client.connect();

  const builtTx = await client.autofill({
    Account,
    Destination,
    TransactionType: 'Payment',
    Amount: new BigNumber(amount).multipliedBy(BASE_UNIT.RIPPLE).toString()
  });

  await client.disconnect();
  return builtTx as RippleBuiltTx;
}

const buildTokenTx = async (asset: Balance, params: BuildTxParams, node: string): Promise<RippleBuiltTx> => {
  const { from: Account, to: Destination, contractAddress: issuer, amount: value } = params;
  const client = new Client(node);
  await client.connect();

  if (typeof issuer === 'undefined') throw Error('Missing contract address.');

  const builtTx = await client.autofill({
    Account,
    Destination,
    TransactionType: 'Payment',
    Amount: { value, issuer, currency: asset.ticker }
  });

  await client.disconnect();
  return builtTx as RippleBuiltTx;
}

export const mapRippleTxBuilder = async (asset: Balance, params: BuildTxParams, node: string): Promise<RippleBuiltTx> => {
  switch (asset.standard) {
    case 'native':
      return (await buildNativeTx(params, node));
    case 'token':
      return (await buildTokenTx(asset, params, node));
    default:
      throw Error('Unsupported Ripple token standard.');
  }
}
