import React from 'react';

import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Stack from '@mui/material/Stack';

interface CustomTabsProps {
  tabPanels: any[]
  tabLabels: string[]
}

interface TabPanelProps {
  index: number
  children: any
  currentTab: number
}

const TabPanel: React.FC<TabPanelProps> = ({ index, children, currentTab }): any => {
  return currentTab === index && (
    <Stack
      sx={{
        height: '100%',
        overflowY: 'hidden'
      }}
    >
      {children}
    </Stack>
  );
};

const CustomTabs: React.FC<CustomTabsProps> = ({ tabPanels, tabLabels }) => {
  const [currentTab, setCurrentTab] = React.useState(0);

  return (
    <Stack
      sx={{
        height: '100%',
        overflowY: 'hidden'
      }}
    >
      <Tabs
        value={currentTab}
        variant="fullWidth"
        TabIndicatorProps={{
          sx: {
            backgroundColor: 'white'
          }
        }}
        onChange={(_, nextTab) => setCurrentTab(nextTab)}
      >
        {
          tabLabels.map((tabLabel, tabLabelIndex) => (
            <Tab
              key={`custom-tab-${tabLabelIndex}`}
              label={tabLabel}
              sx={{
                textTransform: 'none',
                backgroundColor: '#3a3b4c',
                fontSize: theme => theme.font.size.h5,
                borderTopLeftRadius: tabLabelIndex === 0 ? '4px' : undefined,
                color: currentTab === tabLabelIndex ? 'white !important' : 'gray',
                borderTopRightRadius: tabLabelIndex === tabLabels.length - 1 ? '8px' : undefined
              }}
            />
          ))
        }
      </Tabs>
      {
        tabPanels.map((tabPanel, tabPanelIndex) => (
          <TabPanel key={`custom-tab-panel-${tabPanelIndex}`} currentTab={currentTab} index={tabPanelIndex}>
            {tabPanel}
          </TabPanel>
        ))
      }
    </Stack>
  );
}

export default CustomTabs;
