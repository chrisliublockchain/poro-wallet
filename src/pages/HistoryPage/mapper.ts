import dayjs from 'dayjs';
import BigNumber from 'bignumber.js';
import { TransactionStatus } from '@poro-wallet/core/built/interfaces';

import { History } from '@interfaces/history';
import { PendingTransaction } from '@interfaces/redux';
import { TransactionHistoryItemProps } from '@components/history/TransactionHistoryItem';

export const mapPendingTransactionToItem = (tx: PendingTransaction): TransactionHistoryItemProps => {
  const { hash, amount, createdAt, asset: { icon, ticker } } = tx;
  return {
    hash,
    amount,
    createdAt,
    asset: {
      ticker,
      icon: typeof icon === 'undefined' ? '' : icon
    },
    type: 'pending'
  };
};

export const mapHistoryToItem = (tx: History): TransactionHistoryItemProps => {
  const { hash, amount, status, createdAt, asset: { icon, ticker } } = tx;
  return {
    hash,
    amount,
    createdAt: dayjs(createdAt).unix(),
    asset: {
      ticker,
      icon: typeof icon === 'undefined' ? '' : icon
    },
    type: status === TransactionStatus.Rejected ? 'rejected' : BigNumber(amount).isGreaterThan(0) ? 'deposit' : 'withdrawal'
  };
}
