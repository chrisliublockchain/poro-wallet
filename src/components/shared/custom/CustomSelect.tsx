import React from 'react';

import { grey } from '@mui/material/colors';
import MenuItem from '@mui/material/MenuItem';
import { SxProps } from '@mui/material/styles';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

interface SelectOption {
  value: string
  display: string
}

interface CustomSelectProps {
  value: string
  options: SelectOption[]
  setValue: React.Dispatch<string>
  sx?: SxProps
}

const CustomSelect: React.FC<CustomSelectProps> = ({ value, setValue, options, sx }) => {
  const handleOnChange = (e: SelectChangeEvent<string>): void => {
    setValue(e.target.value);
  }

  return (
    <FormControl sx={{ ...sx }}>
      <Select
        value={value}
        onChange={handleOnChange}
        sx={{
          color: 'white',
          fontSize: '12px',
          '& .MuiSvgIcon-root': {
            color: grey['400']
          }
        }}
      >
        {
          options.map((option, optionIndex) => (
            <MenuItem
              value={option.value}
              key={`select-option-${optionIndex}`}
              sx={{
                fontSize: '12px',
                '&:hover': {
                  backgroundColor: grey['900']
                },
                '&.Mui-selected': {
                  backgroundColor: grey['800'],
                  '&.Mui-focusVisible': {
                    backgroundColor: `${grey['800']} !important`
                  }
                }
              }}
            >
              {option.display}
            </MenuItem>
          ))
        }
      </Select>
    </FormControl>
  );
}

export default CustomSelect;
