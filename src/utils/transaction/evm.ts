/* eslint-disable import/prefer-default-export */
import {
  EvmBuiltTx,
  EvmTokenTransactionParams,
  EvmNativeTransactionParams,
  Erc20TransferFunctionParams
} from '@poro-wallet/core';
import BigNumber from 'bignumber.js';
import { BASE_UNIT } from '@poro-wallet/core/built/constants';
import native from '@poro-wallet/core/built/transaction/evm/build/native';
import { erc20Token } from '@poro-wallet/core/built/transaction/evm/build/token';

import { Balance } from '@interfaces/balance';
import { BuildTxParams } from '@interfaces/transaction';

const buildNativeTx = async (params: BuildTxParams, node: string): Promise<EvmBuiltTx> => {
  const { from, to, amount } = params;
  const finalParams: EvmNativeTransactionParams = {
    to,
    from,
    amount: `0x${new BigNumber(amount).multipliedBy(BASE_UNIT.ETHEREUM).toString(16)}`
  };
  const builtTx = await native(node, finalParams);
  return builtTx;
}

const buildErc20Tx = async (asset: Balance, params: BuildTxParams, node: string): Promise<EvmBuiltTx> => {
  const { from, to, contractAddress, contractFunctionParams } = params;

  if (typeof contractAddress === 'undefined') throw Error('Missing contract address.');
  if (typeof contractFunctionParams === 'undefined') throw Error('Missing contrat function parameters.');

  const { value } = contractFunctionParams;

  if (typeof value === 'undefined') throw Error('Missing amount of token to be sent.');

  const finalParams: EvmTokenTransactionParams<Erc20TransferFunctionParams> = {
    from,
    contractAddress,
    functionParams: {
      to,
      value: `0x${new BigNumber(value).multipliedBy(BigNumber(10).exponentiatedBy(asset.decimals)).toString(16)}`
    }
  }

  const builtTx = await erc20Token(node, finalParams);
  return builtTx;
}

export const mapEvmTxBuilder = async (asset: Balance, params: BuildTxParams, node: string): Promise<EvmBuiltTx> => {
  switch (asset.standard) {
    case 'native':
      return (await buildNativeTx(params, node));
    case 'erc-20':
      return (await buildErc20Tx(asset, params, node));
    default:
      throw Error('Unsupported ERC standard.');
  }
}
