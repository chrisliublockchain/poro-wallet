interface Width {
  avatar: {
    asset: {
      large: string
      normal: string
    }
    action: string
    nativeAsset: {
      large: string
      normal: string
    }
    transactionHistoryType: string
  }
  button: {
    action: string
    backNavigation: string
  }
  icon: {
    backNavigation: string
    bottomNavigationBar: string
  }
  layout: {
    root: string
  }
}

interface WidthOptions {
  avatar?: {
    asset?: {
      large?: string
      normal?: string
    }
    action?: string
    nativeAsset?: {
      large?: string
      normal?: string
    }
    transactionHistoryType?: string
  }
  button?: {
    action?: string
    backNavigation?: string
  }
  icon?: {
    backNavigation?: string
    bottomNavigationBar?: string
  }
  layout?: {
    root?: string
  }
}

interface Height {
  avatar: {
    asset: {
      normal: string
      large: string
    }
    action: string
    nativeAsset: {
      normal: string
      large: string
    }
    transactionHistoryType: string
  }
  button: {
    action: string
    backNavigation: string
  }
  icon: {
    backNavigation: string
    bottomNavigationBar: string
  }
  layout: {
    root: string
    rootLogoBar: string
    headerNavigationBar: string
    bottomNavigationBar: string
  }
}

interface HeightOptions {
  avatar?: {
    asset?: {
      normal?: string
      large?: string
    }
    action?: string
    nativeAsset?: {
      normal?: string
      large?: string
    }
    transactionHistoryType?: string
  }
  button?: {
    action?: string
    backNavigation?: string
  }
  icon: {
    backNavigation?: string
    bottomNavigationBar?: string
  }
  layout?: {
    root?: string
    rootLogoBar?: string
    headerNavigationBar?: string
    bottomNavigationBar?: string
  }
}

export interface ISizingTheme {
  width: Width
  height: Height
}

export interface ISizingThemeOptions {
  width?: WidthOptions
  height?: HeightOptions
}
