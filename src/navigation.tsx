import { ReactElement } from 'react';
import WalletIcon from '@mui/icons-material/Wallet';
import CompareArrowsIcon from '@mui/icons-material/CompareArrows';
import ReceiptLongOutlinedIcon from '@mui/icons-material/ReceiptLongOutlined';

import HomePage from '@pages/HomePage';
import AssetPage from '@pages/AssetPage';
import HistoryPage from '@pages/HistoryPage';
import SendTokenPage from '@pages/SendTokenPage';

export interface INavigationElement {
  path: string
  component: ReactElement<any, any>
  // label and icon fields only exist for bottom navigation tab component
  label?: string
  icon?: ReactElement<any, any>
}

const navigations: INavigationElement[] = [{
  path: '/',
  label: 'Home',
  component: <HomePage />,
  icon: <WalletIcon />
}, {
  path: '/transfer',
  label: 'Transfer',
  component: <HomePage />,
  icon: <CompareArrowsIcon />
}, {
  path: '/history',
  label: 'History',
  component: <HistoryPage />,
  icon: <ReceiptLongOutlinedIcon />
}, {
  path: '/asset',
  component: <AssetPage />
}, {
  path: '/send/token',
  component: <SendTokenPage />
}];

export default navigations;
