import React from 'react';
import { isEvmAddress, isRippleAddress } from '@poro-wallet/core/built/utils/validation';

import { Balance } from '@interfaces/balance';
import { BlockchainName } from '@interfaces/blockchain';

// export const isEvmAddress = (address: string): boolean => utils.isAddress(address);
// export const isRippleAddress = (address: string): boolean => new RegExp(`^r[${RIPPLE_ALLOWED_CHARS}]{27,35}$`).test(address);

export const recipientValidation = (blockchain: BlockchainName) => (recipient: string): boolean | string => {
  switch (blockchain) {
    case 'goerli':
    case 'mumbai':
      return isEvmAddress(recipient) || 'Please input a valid address';
    case 'ripple-testnet':
      return isRippleAddress(recipient) || 'Please input a valid address';
    default:
      throw Error('Unsupported blockchain.');
  }
}

export const amountValidation = (amount: string, asset?: Balance): boolean | string => {
  return true;
}

export const setPostSuccessRecipientValidation = (
  recipient: string,
  setRecipient: React.Dispatch<string>,
  setValidRecipient: React.Dispatch<boolean>
): void => {
  setRecipient(recipient);
  setValidRecipient(true);
}

export const setPostSuccessAmountValidation = (
  amount: string,
  setAmount: React.Dispatch<string>,
  setValidAmount: React.Dispatch<boolean>
): void => {
  setAmount(amount);
  setValidAmount(true);
}

export const setPostFailRecipientValidation = (
  recipient: string,
  setRecipient: React.Dispatch<string>,
  setValidRecipient: React.Dispatch<boolean>
): void => {
  setRecipient(recipient);
  setValidRecipient(false);
}

export const setPostFailAmountValidation = (
  amount: string,
  setAmount: React.Dispatch<string>,
  setValidAmount: React.Dispatch<boolean>
): void => {
  setAmount(amount);
  setValidAmount(false);
}
