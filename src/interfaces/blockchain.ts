export type BlockchainName = 'goerli' | 'mumbai' | 'ripple-testnet' | 'stellar-testnet' | 'theta';

export interface Blockchain {
  node: string
  displayName: string
  name: BlockchainName
  nodeWss?: string
}
