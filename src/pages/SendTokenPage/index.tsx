/* eslint-disable no-promise-executor-return */
/* eslint-disable @typescript-eslint/no-misused-promises */
import React from 'react';
import dayjs from 'dayjs';
import { useSnackbar } from 'notistack';
import { useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';

import Stack from '@mui/material/Stack';
import { grey } from '@mui/material/colors';
import CircularProgress from '@mui/material/CircularProgress';
import RootContainer from '@components/layouts/RootContainer';
import CustomAvatar from '@components/shared/custom/CustomAvatar';
import CustomButton from '@components/shared/custom/CustomButton';
import CustomInputField from '@components/shared/custom/CustomInputField';
import CustomTypography from '@components/shared/custom/CustomTypography';
import HeaderNavigationBar from '@components/layouts/HeaderNavigationBar';

import { Dispatch } from '@redux/index';
import { buildTx } from '@utils/transaction';
import { Balance } from '@interfaces/balance';
import useFeeLoader from '@hooks/useFeeLoader';
import { signTx, sendTx } from '@apis/transaction';
import { insertPendingTx } from '@redux/services/pendingTransactions';
import { useGetSupportedBlockchainsQuery } from '@redux/services/blockchains';

import {
  amountValidation,
  recipientValidation,
  setPostFailAmountValidation,
  setPostFailRecipientValidation,
  setPostSuccessAmountValidation,
  setPostSuccessRecipientValidation
} from './validation';

const SendTokenPage: React.FC = () => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const {
    data: blockchains = [],
    isLoading: isLoadingSupportedBlockchain
  } = useGetSupportedBlockchainsQuery();
  const { state }: { state: { asset: Balance, nativeAsset?: Balance } | null } = useLocation();
  const { fee, isLoading: isLoadingFee } = useFeeLoader(state?.asset.blockchain);

  const [amount, setAmount] = React.useState<string>('');
  const [recipient, setRecipient] = React.useState<string>('');
  const [validAmount, setValidAmount] = React.useState<boolean>(false);
  const [validRecipient, setValidRecipient] = React.useState<boolean>(false);

  const feeTicker = React.useMemo(() => {
    if (state !== null) {
      const { asset, nativeAsset } = state;
      if (typeof nativeAsset !== 'undefined') {
        return nativeAsset.ticker;
      }
      return asset.ticker;
    }
    return '';
  }, [state]);

  const onConfirm = async (): Promise<void> => {
    if (blockchains.length === 0 || state === null) {
      throw Error('Invalid data on balance and corresponding blockchain information.');
    }

    const assetBlockchain = blockchains.filter((blockchain) => blockchain.name === state.asset.blockchain);
    if (assetBlockchain.length === 0) {
      throw Error('Underlying blockchain for this asset is not supported.');
    }

    const node = state.asset.blockchain === 'ripple-testnet' ? String(assetBlockchain[0].nodeWss) : assetBlockchain[0].node;
    try {
      const builtTx = await buildTx(
        state.asset,
        {
          amount,
          to: recipient,
          from: state.asset.wallet,
          contractAddress: state.asset.address,
          contractFunctionParams: { to: recipient, value: amount }
        },
        node
      );
      console.log('Finished transaction construction', { builtTx });

      const signedTx = await signTx(state.asset.blockchain, builtTx);
      console.log('Finished transaction signing', { signedTx });

      const hash = await sendTx(state.asset.blockchain, signedTx);
      console.log('Finished transaction boardcasting', { hash });

      dispatch(insertPendingTx({
        hash,
        amount,
        recipient,
        asset: state.asset,
        createdAt: dayjs().unix()
      }));

      enqueueSnackbar(
        `Successfully sent ${amount} to ${recipient}`,
        { variant: 'success', autoHideDuration: 3000 }
      );

      await new Promise(resolve => setTimeout(resolve, 3000));
      navigate('/history');
    } catch (error: any) {
      enqueueSnackbar(`Encountered error while sending transaction: ${error.message}`, { variant: 'error' });
    }
  }

  return (
    <RootContainer>
      <Stack width="100%" height="100%" alignItems="center">
        <HeaderNavigationBar
          title={state === null ? undefined : `Send ${state.asset.ticker}`}
          onClick={() => navigate('/asset', { state })}
        />
        {
          state === null
            ? (
                <Stack height="100%" justifyContent="center">
                  <CircularProgress sx={{ color: 'white' }} />
                </Stack>
              )
            : (
                <Stack
                  px={3}
                  pt={1}
                  pb={3}
                  width="100%"
                  height="100%"
                  boxSizing="border-box"
                  justifyContent="space-between"
                >
                  <Stack spacing={2} width="100%" alignItems="center">
                    <CustomAvatar
                      size='large'
                      asset={{ displayName: state.asset.displayName, icon: state.asset.icon ?? '' }}
                      nativeAsset={
                        typeof state.nativeAsset !== 'undefined'
                          ? { displayName: state.nativeAsset.displayName, icon: state.nativeAsset.icon ?? '' }
                          : undefined
                      }
                    />
                    <CustomInputField
                      label="Recipient"
                      value={recipient}
                      validation={recipientValidation(state.asset.blockchain)}
                      postFailValidation={(input: string) => setPostFailRecipientValidation(input, setRecipient, setValidRecipient)}
                      postSuccessValidation={(input: string) => setPostSuccessRecipientValidation(input, setRecipient, setValidRecipient)}
                    />
                    <CustomInputField
                      label="Amount"
                      value={amount}
                      validation={amountValidation}
                      postFailValidation={(input: string) => setPostFailAmountValidation(input, setAmount, setValidAmount)}
                      postSuccessValidation={(input: string) => setPostSuccessAmountValidation(input, setAmount, setValidAmount)}
                    />
                  </Stack>
                  <Stack width="100%" alignItems="center">
                    <Stack
                      mb={1}
                      px={2}
                      py="12px"
                      width="100%"
                      direction="row"
                      boxSizing="border-box"
                      justifyContent="space-between"
                      sx={{
                        backgroundColor: grey['900']
                      }}
                    >
                      <CustomTypography variant='h6' sx={{ color: 'white' }}>
                        Fee
                      </CustomTypography>
                      <CustomTypography variant='h6' sx={{ color: grey['600'] }}>
                        {(isLoadingFee || typeof fee === 'undefined') && 'Loading...'}
                        {!isLoadingFee && typeof fee !== 'undefined' && `${fee.fee} ${feeTicker}`}
                      </CustomTypography>
                    </Stack>
                    <CustomButton
                      onClick={onConfirm}
                      label={isLoadingSupportedBlockchain ? 'Connecting blockchain...' : 'Confirm'}
                      disabled={!validAmount || !validRecipient || isLoadingSupportedBlockchain || isLoadingFee}
                    />
                  </Stack>
                </Stack>
              )
        }
      </Stack>
    </RootContainer>
  )
}

export default SendTokenPage;
