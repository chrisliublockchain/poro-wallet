import React from 'react';

import Stack from '@mui/material/Stack';
import Skeleton from '@mui/material/Skeleton';

const BalanceListSkeleton: React.FC = () => {
  return (
    <Stack direction="row" alignItems="center">
      <Skeleton
        width={30}
        height={30}
        animation="wave"
        variant="circular"
      />
      <Stack ml={2} flex={4}>
        <Skeleton
          height={20}
          animation="wave"
          variant="rectangular"
        />
        <Skeleton
          height={20}
          animation="wave"
          variant="rectangular"
          sx={{ mt: 1 }}
        />
      </Stack>
      <Skeleton
        height={20}
        animation="wave"
        variant="rectangular"
        sx={{ ml: 2, flex: 1 }}
      />
    </Stack>
  );
};

export default BalanceListSkeleton;
