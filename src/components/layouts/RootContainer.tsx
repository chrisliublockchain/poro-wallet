import React from 'react';

import Stack from '@mui/material/Stack';
import { useTheme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import RootLogoBar from '@components/layouts/RootLogoBar';
import NavigationBar from '@components/layouts/NavigationBar';

interface RootContainerProps {
  children: any
}

const RootContainer: React.FC<RootContainerProps> = ({ children }) => {
  const theme = useTheme();

  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: theme.color.background.external
      }}
    >
      <Stack
        mx="auto"
        borderRadius={2}
        overflow="hidden"
        width={theme.sizing.width.layout.root}
        height={theme.sizing.height.layout.root}
      >
        <RootLogoBar />
        <Stack
          sx={{
            backgroundColor: theme.color.background.internal,
            height: `calc(100% - ${theme.sizing.height.layout.rootLogoBar} - ${theme.sizing.height.layout.bottomNavigationBar})`
          }}
        >
          {children}
        </Stack>
        <NavigationBar />
      </Stack>
    </Container>
  );
};

export default RootContainer;
