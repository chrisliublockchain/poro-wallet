import { BlockchainName } from './blockchain';

// Interfaces
export interface Balance {
  amount: string
  ticker: string
  wallet: string
  address: string
  decimals: number
  standard: string
  displayName: string
  blockchain: BlockchainName
  icon?: string
}

export interface NativeAssetHashMap {
  [blockchain: string]: Balance
}

// Props
export interface BalanceListProps {
  error: boolean
  loading: boolean
  balances: Balance[]
  nativeAssetHashmap: NativeAssetHashMap
}

export interface BalanceListItemProps {
  balance: Balance
  nativeAsset?: Balance
}
