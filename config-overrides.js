const webpack = require('webpack');
const { aliasWebpack, aliasJest } = require('react-app-alias');

const options = {};

module.exports = function override (config) {
  const modifiedConfig = aliasWebpack(options)(config);

  modifiedConfig.resolve.fallback = {
    "tls": false,
    "net": false,
    "url": false,
    "fs": require.resolve("browserify-fs"),
    "os": require.resolve("os-browserify"),
    "http": require.resolve("stream-http"),
    "path": require.resolve("path-browserify"),
    "https": require.resolve("https-browserify"),
    "stream": require.resolve("stream-browserify"),
    "crypto": require.resolve("crypto-browserify"),
  };

  modifiedConfig.plugins.push(
    new webpack.ProvidePlugin({
      process: 'process/browser',
      Buffer: ['buffer', 'Buffer']
    })
  );

  return modifiedConfig;
};

module.exports.jest = aliasJest(options);
