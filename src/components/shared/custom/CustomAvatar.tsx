import React from 'react';

import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';

interface AssetAvatarDetails {
  icon: string
  displayName: string
}

interface CustomAvatarProps {
  asset: AssetAvatarDetails
  size?: 'normal' | 'large'
  nativeAsset?: AssetAvatarDetails
}

const CustomAvatar: React.FC<CustomAvatarProps> = ({ asset, nativeAsset, size = 'normal' }) => {
  return typeof nativeAsset !== 'undefined'
    ? (
        <Badge
          overlap="circular"
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          badgeContent={(
            <Avatar
              src={nativeAsset.icon}
              alt={nativeAsset.displayName}
              sx={{
                width: theme => theme.sizing.width.avatar.nativeAsset[size],
                height: theme => theme.sizing.height.avatar.nativeAsset[size],
                border: theme => `1px solid ${theme.palette.background.paper}`
              }}
            />
          )}
        >
          <Avatar
            src={asset.icon === '' ? undefined : asset.icon}
            alt={asset.displayName}
            sx={{
              color: 'black',
              fontWeight: 'bold',
              width: theme => theme.sizing.height.avatar.asset[size],
              height: theme => theme.sizing.height.avatar.asset[size]
            }}
          >
            {asset.icon === '' && asset.displayName.charAt(0)}
          </Avatar>
        </Badge>
      )
    : (
        <Avatar
          src={asset.icon}
          alt={asset.displayName}
          sx={{
            color: 'black',
            fontWeight: 'bold',
            width: theme => theme.sizing.height.avatar.asset[size],
            height: theme => theme.sizing.height.avatar.asset[size]
          }}
        />
      );
};

export default CustomAvatar;
