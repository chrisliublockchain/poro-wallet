export interface AddHistoryRequestParams {
  hash: string
  status: string
  amount: string
  sender: string
  ticker: string
  recipient: string
  blockchain: string
}
