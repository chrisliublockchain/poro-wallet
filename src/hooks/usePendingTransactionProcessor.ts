/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-promise-executor-return */
/* eslint-disable @typescript-eslint/no-misused-promises */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import transaction from '@poro-wallet/core/built/transaction';
import { TransactionStatus } from '@poro-wallet/core/built/interfaces';

import { Dispatch } from '@redux/index';
import { addHistory } from '@apis/history';
import { BlockchainName } from '@interfaces/blockchain';
import { selectPendingTransactions } from '@redux/selector';
import { PendingTransactionProcessorState } from '@interfaces/hook';
import { deletePendingTx } from '@redux/services/pendingTransactions';
import { useGetSupportedBlockchainsQuery } from '@redux/services/blockchains';

const mapStatusFunction = (blockchain: BlockchainName): 'evm' => {
  switch (blockchain) {
    case 'goerli':
    case 'mumbai':
      return 'evm';
    default:
      throw Error('Unsupported blockchain.');
  }
}

const usePendingTransactionProcessor = (refetchHistory: () => void): PendingTransactionProcessorState => {
  const dispatch: Dispatch = useDispatch();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const { data: blockchains = [] } = useGetSupportedBlockchainsQuery();
  const pending = useSelector(selectPendingTransactions);

  React.useEffect(() => {
    const syncHistoryWithDatabase = async (): Promise<void> => {
      setIsLoading(true);

      for (const tx of pending) {
        const { hash, amount, asset: { wallet: sender, ticker, blockchain }, recipient } = tx;

        const { node } = blockchains.filter((b) => b.name === blockchain)[0];
        const status = await transaction[mapStatusFunction(blockchain)].status(node, [hash]);
        if (status[hash] === TransactionStatus.Accepted || status[hash] === TransactionStatus.Rejected) {
          const added = await addHistory({
            hash,
            sender,
            ticker,
            recipient,
            blockchain,
            amount: `-${amount}`,
            status: status[hash]
          });
          if (added) {
            dispatch(deletePendingTx(hash));
            refetchHistory();
          }
        }
      }

      setIsLoading(false);
    };

    const syncHistoryInterval = setInterval(async () => await syncHistoryWithDatabase(), 10000);
    return () => clearInterval(syncHistoryInterval);
  }, [blockchains, pending, refetchHistory]);

  return { isLoading, pending };
}

export default usePendingTransactionProcessor;
