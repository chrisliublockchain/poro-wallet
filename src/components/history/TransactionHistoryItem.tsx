/* eslint-disable react/display-name */
import React from 'react';

import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import { SxProps } from '@mui/material/styles';
import PauseIcon from '@mui/icons-material/Pause';
import ClearIcon from '@mui/icons-material/Clear';
import { blue, green, yellow } from '@mui/material/colors';
import ForwardRefWrapper from '@components/shared/ForwardRefWrapper';
import CallMadeOutlinedIcon from '@mui/icons-material/CallMadeOutlined';
import CustomTypography from '@components/shared/custom/CustomTypography';
import CallReceivedOutlinedIcon from '@mui/icons-material/CallReceivedOutlined';

import { toDisplayAmount, toDisplayTransactionHash, toDisplayTransactionHistoryTime } from '@utils/formatting';

type TransactionHistoryType = 'withdrawal' | 'deposit' | 'rejected' | 'pending';

export interface TransactionHistoryItemProps {
  hash: string
  amount: string
  createdAt: number // In unix number format
  asset: {
    icon: string
    ticker: string
  }
  type: TransactionHistoryType
  sx?: SxProps
  assignedRef?: React.ForwardedRef<any>
}

const getColorByTransactionHistoryType = (type: TransactionHistoryType): string => {
  return type === 'deposit'
    ? green['600']
    : type === 'withdrawal'
      ? blue['600']
      : type === 'pending'
        ? yellow['600']
        : 'red';
}

const getDisplayStatusByTransactionHistoryType = (type: TransactionHistoryType): string => {
  return type === 'deposit'
    ? 'Received'
    : type === 'withdrawal'
      ? 'Sent'
      : type === 'pending'
        ? 'Pending'
        : 'Rejected';
}

const TransactionHistoryItem: React.FC<TransactionHistoryItemProps> = ({ hash, amount, createdAt, asset, type, sx, assignedRef }) => {
  return (
    <Stack ref={assignedRef} direction="row" alignItems="center" sx={{ ...sx }} >
      <Avatar
        sx={{
          backgroundColor: 'transparent',
          color: getColorByTransactionHistoryType(type),
          border: `1px solid ${getColorByTransactionHistoryType(type)}`,
          width: theme => theme.sizing.width.avatar.transactionHistoryType,
          height: theme => theme.sizing.height.avatar.transactionHistoryType
        }}
      >
        {type === 'pending' && <PauseIcon />}
        {type === 'rejected' && <ClearIcon />}
        {type === 'withdrawal' && <CallMadeOutlinedIcon />}
        {type === 'deposit' && <CallReceivedOutlinedIcon />}
      </Avatar>
      <Stack ml={2} flex={3}>
        <CustomTypography variant='h5' sx={{ mb: '4px', color: 'white' }}>
          {toDisplayTransactionHash(hash)}
        </CustomTypography>
        <CustomTypography variant='h6' sx={{ color: 'gray' }}>
          {`${getDisplayStatusByTransactionHistoryType(type)} • ${toDisplayTransactionHistoryTime(createdAt)}`}
        </CustomTypography>
      </Stack>
      <CustomTypography variant='h5' sx={{ color: 'gray', flex: 1, textAlign: 'right', whiteSpace: 'nowrap' }}>
        {`${toDisplayAmount(amount)} ${asset.ticker}`}
      </CustomTypography>
    </Stack>
  );
}

export const TransactionHistoryItemWithRef = ForwardRefWrapper(TransactionHistoryItem);

export default TransactionHistoryItem;
