import React from 'react';

import Stack from '@mui/material/Stack';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import CustomTypography from '@components/shared/custom/CustomTypography';

interface HeaderNavigationBarProps {
  onClick: () => void
  title?: string
}

const HeaderNavigationBar: React.FC<HeaderNavigationBarProps> = ({ title, onClick }) => {
  const theme = useTheme();

  return (
    <Stack
      width="100%"
      direction="row"
      alignItems="center"
      height={theme.sizing.height.layout.headerNavigationBar}
    >
      <Stack flex={1}>
        <IconButton
          onClick={onClick}
          sx={{
            p: 0,
            color: 'white',
            backgroundColor: 'unset',
            width: theme.sizing.width.button.backNavigation,
            height: theme.sizing.height.button.backNavigation,
            '&:hover': {
              backgroundColor: 'unset'
            }
          }}
        >
          <ArrowBackIcon
            sx={{
              width: theme.sizing.width.icon.backNavigation,
              height: theme.sizing.height.icon.backNavigation
            }}
          />
        </IconButton>
      </Stack>
      <CustomTypography variant='h4' sx={{ color: 'white' }}>
        {title}
      </CustomTypography>
      <Stack flex={1} />
    </Stack>
  );
};

export default HeaderNavigationBar;
