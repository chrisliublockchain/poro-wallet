import { Balance } from './balance';

export interface PendingTransaction {
  hash: string
  amount: string // Decimal
  asset: Balance
  createdAt: number
  recipient: string
}
