/* eslint-disable import/prefer-default-export */
import storage from 'redux-persist/lib/storage';
import {
  FLUSH,
  PAUSE,
  PURGE,
  PERSIST,
  REGISTER,
  REHYDRATE,
  persistStore,
  persistReducer
} from 'redux-persist';
import { configureStore, combineReducers } from '@reduxjs/toolkit';

import { balancesApi } from './services/balances';
import { historiesApi } from './services/histories';
import { blockchainsApi } from './services/blockchains';
import { pendingTransactionsSlice } from './services/pendingTransactions';

const combinedReducers = combineReducers({
  [balancesApi.reducerPath]: balancesApi.reducer,
  [historiesApi.reducerPath]: historiesApi.reducer,
  [blockchainsApi.reducerPath]: blockchainsApi.reducer,
  [pendingTransactionsSlice.name]: pendingTransactionsSlice.reducer
});

const reducer = persistReducer({
  storage,
  key: 'root',
  whitelist: ['pendingTransactions']
}, combinedReducers)

export const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, PAUSE, PURGE, PERSIST, REGISTER, REHYDRATE]
    }
  })
    .concat(balancesApi.middleware)
    .concat(historiesApi.middleware)
    .concat(blockchainsApi.middleware)
});

export const persistor = persistStore(store);

export type Dispatch = typeof store.dispatch;
export type State = ReturnType<typeof store.getState>;
