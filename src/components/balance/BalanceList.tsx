import React from 'react';

import Stack from '@mui/material/Stack';
import CustomTypography from '@components/shared/custom/CustomTypography';

import { BalanceListProps } from '@interfaces/balance';

import BalanceListItem from './BalanceListItem';
import BalanceListSkeleton from './BalanceListSkeleton';

const BalanceList: React.FC<BalanceListProps> = ({ balances, nativeAssetHashmap, loading, error }) => {
  return (
    <Stack
      py={2}
      px={3}
      spacing={2}
      height="100%"
      sx={{
        overflowY: 'auto'
      }}
    >
      {
        error && (
          <CustomTypography variant="h5" sx={{ color: 'red', fontWeight: 'bold', textAlign: 'center' }}>
            Encountering unexpected error while fetching asset balances. Will retry in a few seconds.
          </CustomTypography>
        )
      }
      {
        (error || loading)
          ? (
              [1, 2, 3].map((_, skeletonIndex) => (
                <BalanceListSkeleton key={`balance-list-skeleton-${skeletonIndex}`} />
              ))
            )
          : (
              balances.map((balance, balanceIndex) => (
                <BalanceListItem
                  balance={balance}
                  key={`balance-list-item-${balanceIndex}`}
                  nativeAsset={balance.standard !== 'native' ? nativeAssetHashmap[balance.blockchain] : undefined}
                />
              ))
            )
      }
    </Stack>
  );
};

export default BalanceList;
