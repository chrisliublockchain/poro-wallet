import React from 'react';

import Stack from '@mui/material/Stack';
import RootContainer from '@components/layouts/RootContainer';
import TransactionHistoryPendingList from '@components/history/TransactionHistoryPendingList';

import { useGetHistoriesQuery } from '@redux/services/histories';
import usePendingTransactionProcessor from '@hooks/usePendingTransactionProcessor';

import TransactionHistoryList from '@components/history/TransactionHistoryList';

const HistoryPage: React.FC = () => {
  const { data: histories = [], isLoading, refetch: refetchHistory } = useGetHistoriesQuery();
  const { pending } = usePendingTransactionProcessor(refetchHistory);

  return (
    <RootContainer>
      <Stack height="100%" py={2} sx={{ overflowY: 'auto', overflowX: 'hidden' }}>
        {pending.length > 0 && <TransactionHistoryPendingList pendingTransactions={pending} />}
        <TransactionHistoryList
          histories={histories}
          isLoading={isLoading}
          sx={{ mt: pending.length > 0 ? 4 : 0 }}
        />
      </Stack>
    </RootContainer>
  );
}

export default HistoryPage;
