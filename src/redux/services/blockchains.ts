/* eslint-disable @typescript-eslint/no-invalid-void-type */
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { Blockchain } from '@interfaces/blockchain';

export const blockchainsApi = createApi({
  reducerPath: 'blockchains',
  baseQuery: fetchBaseQuery({ baseUrl: `${String(process.env.REACT_APP_PORO_WALLET_BACKEND)}/v1` }),
  endpoints: (builder) => ({
    getSupportedBlockchains: builder.query<Blockchain[], void>({
      query: () => '/blockchain',
      transformResponse: (response: { success: boolean, blockchains: Blockchain[] }) => response.blockchains
    })
  })
})

export const { useGetSupportedBlockchainsQuery } = blockchainsApi;
