import { TransactionStatus } from '@poro-wallet/core/built/interfaces';

export interface History {
  hash: string
  amount: string
  sender: string
  recipient: string
  createdAt: string
  asset: {
    ticker: string
    decimals: number
    icon?: string
  }
  status: TransactionStatus
}
