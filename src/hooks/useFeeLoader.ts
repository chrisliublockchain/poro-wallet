/* eslint-disable no-promise-executor-return */
/* eslint-disable @typescript-eslint/no-misused-promises */
import React from 'react';
import { Client } from 'xrpl';
import BigNumber from 'bignumber.js';
import price from '@poro-wallet/core/built/price';
import { BASE_UNIT, ETHEREUM_DEFAULT_GAS_LIMIT } from '@poro-wallet/core/built/constants';

import { Fee, FeeLoader } from '@interfaces/hook';
import { BlockchainName } from '@interfaces/blockchain';
import { useGetSupportedBlockchainsQuery } from '@redux/services/blockchains';

const mapPriceFunction = (blockchain: BlockchainName): 'evm' | 'ripple' => {
  switch (blockchain) {
    case 'goerli':
    case 'mumbai':
      return 'evm';
    case 'ripple-testnet':
      return 'ripple';
    default:
      throw Error('Unsupported blockchain.');
  }
}

const useFeeLoader = (blockchain?: BlockchainName): FeeLoader => {
  const [fee, setFee] = React.useState<Fee>();
  const [isLoading, setIsLoading] = React.useState<boolean>(true);
  const { data: blockchains = [] } = useGetSupportedBlockchainsQuery();

  React.useEffect(() => {
    const fetchFee = async (): Promise<void> => {
      setIsLoading(true);
      if (typeof blockchain !== 'undefined') {
        const priceFunction = mapPriceFunction(blockchain);
        const { node, nodeWss } = blockchains.filter((b) => b.name === blockchain)[0];

        const rippleClient = priceFunction === 'ripple' ? new Client(nodeWss as string) : undefined;

        try {
          if (priceFunction === 'evm') {
            const { maxFeePerGas = '0x0', maxPriorityFeePerGas = '0x0' } = await price[priceFunction].fee(node);
            setFee({
              fee: new BigNumber(maxFeePerGas)
                .multipliedBy(ETHEREUM_DEFAULT_GAS_LIMIT)
                .dividedBy(BASE_UNIT.ETHEREUM)
                .toString(),
              maxFeePerGas,
              maxPriorityFeePerGas
            });
          } else if (priceFunction === 'ripple' && typeof rippleClient !== 'undefined') {
            await rippleClient.connect();
            const fetchedFee = await rippleClient.request({ command: 'fee' });
            setFee({ fee: BigNumber(fetchedFee.result.drops.base_fee).multipliedBy(rippleClient.feeCushion).toString() });
          } else {
            const fetchedFee = await (price[priceFunction] as any).fee(node);
            setFee({ fee: fetchedFee });
          }
          setIsLoading(false);
        } catch (error: any) {
          setFee({ fee: '0' });
          setIsLoading(false);
        }
      }
    }

    const fetchFeeInterval = setInterval(async () => await fetchFee(), 5000);
    return () => clearInterval(fetchFeeInterval);
  }, [blockchains]);

  return { fee, isLoading };
}

export default useFeeLoader;
