import React from 'react';
import { useNavigate } from 'react-router-dom';

import Stack from '@mui/material/Stack';
import CustomAvatar from '@components/shared/custom/CustomAvatar';
import CustomTypography from '@components/shared/custom/CustomTypography';

import { BalanceListItemProps } from '@interfaces/balance';
import { toDisplayBalance } from '@utils/formatting';

const BalanceListItem: React.FC<BalanceListItemProps> = ({ balance, nativeAsset }) => {
  const navigate = useNavigate();

  return (
    <Stack
      onClick={() => navigate('/asset', { state: { asset: balance, nativeAsset } })}
      direction="row"
      alignItems="center"
      sx={{
        '&:hover': {
          cursor: 'pointer'
        }
      }}
    >
      <CustomAvatar
        asset={{ displayName: balance.displayName, icon: balance.icon ?? '' }}
        nativeAsset={
          typeof nativeAsset !== 'undefined'
            ? { displayName: nativeAsset.displayName, icon: nativeAsset.icon ?? '' }
            : undefined
        }
      />
      <Stack ml={2} flex={3}>
        <CustomTypography variant='h6' sx={{ mb: '4px', color: 'white' }}>
          {balance.ticker}
        </CustomTypography>
        <CustomTypography variant='p' sx={{ color: 'gray' }}>
          {balance.displayName}
        </CustomTypography>
      </Stack>
      <CustomTypography variant='h5' sx={{ color: 'gray', flex: 1, textAlign: 'right' }}>
        {toDisplayBalance(balance)}
      </CustomTypography>
    </Stack>
  );
}

export default BalanceListItem;
