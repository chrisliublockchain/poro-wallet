/* eslint-disable @typescript-eslint/no-invalid-void-type */
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { History } from '@interfaces/history';

export const historiesApi = createApi({
  reducerPath: 'histories',
  baseQuery: fetchBaseQuery({ baseUrl: `${String(process.env.REACT_APP_PORO_WALLET_BACKEND)}/v1` }),
  endpoints: (builder) => ({
    getHistories: builder.query<History[], void>({
      query: () => '/history',
      transformResponse: (response: { success: boolean, histories: History[] }) => response.histories
    })
  })
})

export const { useGetHistoriesQuery } = historiesApi;
