/* eslint-disable import/prefer-default-export */
import dayjs from 'dayjs';
import BigNumber from 'bignumber.js';
import relativeTime from 'dayjs/plugin/relativeTime';

import { Balance } from '@interfaces/balance';

export const toDisplayTransactionHistoryTime = (createdAt: number): string => {
  dayjs.extend(relativeTime);
  const input = dayjs.unix(createdAt);
  const difference = dayjs().diff(input);
  const isGreaterThanOneDay = difference > 24 * 60 * 60 * 1000;
  if (isGreaterThanOneDay) {
    return input.format('MMM DD, YYYY');
  }
  return dayjs().to(input);
}

export const toDisplayAmount = (amount: string): string => new BigNumber(amount).abs().toFixed(4);

export const toDisplayTransactionHash = (hash: string): string => hash.length > 14 ? `${hash.slice(0, 10)}...${hash.slice(-4)}` : hash;

export const toDisplayBalance = (balance: Balance): string => new BigNumber(balance.amount).dividedBy(10 ** balance.decimals).toFixed(4);
