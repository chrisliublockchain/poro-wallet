/* eslint-disable @typescript-eslint/no-invalid-void-type */
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { Balance } from '@interfaces/balance';

export const balancesApi = createApi({
  reducerPath: 'balances',
  baseQuery: fetchBaseQuery({ baseUrl: `${String(process.env.REACT_APP_PORO_WALLET_BACKEND)}/v1` }),
  endpoints: (builder) => ({
    getBalances: builder.query<Balance[], void>({
      query: () => '/balance',
      transformResponse: (response: { success: boolean, balances: Balance[] }) => response.balances
    })
  })
})

export const { useGetBalancesQuery } = balancesApi;
