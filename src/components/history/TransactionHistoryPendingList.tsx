/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import Stack from '@mui/material/Stack';
import Slide from '@mui/material/Slide';
import { PendingTransaction } from '@interfaces/redux';
import { CustomTypographyWithRef } from '@components/shared/custom/CustomTypography';
import { TransactionHistoryItemWithRef } from '@components/history/TransactionHistoryItem';

import { mapPendingTransactionToItem } from '@pages/HistoryPage/mapper';

interface TransactionHistoryPendingListProps {
  pendingTransactions: PendingTransaction[]
}

const TransactionHistoryPendingList: React.FC<TransactionHistoryPendingListProps> = ({ pendingTransactions }) => {
  const pendingTxListRef = React.useRef();

  return (
    <Stack px={3} ref={pendingTxListRef}>
      <Slide
        in
        mountOnEnter
        unmountOnExit
        direction="left"
        container={pendingTxListRef.current}
      >
        <CustomTypographyWithRef variant='h3' sx={{ mb: 2, color: 'white' }}>
          Pending
        </CustomTypographyWithRef>
      </Slide>
      <Stack>
        {
          pendingTransactions.map((pendingTx, pendingTxIndex) => (
            <Slide
              in
              mountOnEnter
              unmountOnExit
              direction="left"
              container={pendingTxListRef.current}
              key={`pending-transaction-history-item-${pendingTxIndex}`}
            >
              <TransactionHistoryItemWithRef
                {...mapPendingTransactionToItem(pendingTx)}
                sx={{ mt: pendingTxIndex !== 0 ? 1 : 0 }}
              />
            </Slide>
          ))
        }
      </Stack>
    </Stack>
  );
}

export default TransactionHistoryPendingList;
