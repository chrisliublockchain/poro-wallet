import { EvmBuiltTx, RippleBuiltTx, TransferFunctionParams } from '@poro-wallet/core';

export type BuiltTx = EvmBuiltTx | RippleBuiltTx;

export interface BuildTxParams {
  to: string
  from: string
  amount: string
  contractAddress?: string
  contractFunctionParams?: TransferFunctionParams // EVM
}
