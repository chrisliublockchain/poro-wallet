/* eslint-disable react/display-name */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

type ForwardRefWrapperType<T> = React.ForwardRefExoticComponent<React.PropsWithoutRef<T> & React.RefAttributes<any>>;

const ForwardRefWrapper = <T extends {}>(
  Component: React.FC<T>
): ForwardRefWrapperType<T> => React.forwardRef<any, T>((props, ref) => (
    <Component {...props} assignedRef={ref} />
  ));

export default ForwardRefWrapper;
