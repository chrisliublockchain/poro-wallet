import React from 'react';

import Stack from '@mui/material/Stack';
import { blue } from '@mui/material/colors';
import { SxProps } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CustomTypography from '@components/shared/custom/CustomTypography';

interface Action {
  name: string
  icon: JSX.Element
  onClick: () => void
}

interface ActionBarProps {
  actions: Action[]
  sx?: SxProps
}

const ActionBar: React.FC<ActionBarProps> = ({ actions, sx }) => {
  return (
    <Stack direction="row" justifyContent="space-evenly" sx={{ ...sx }}>
      {
        actions.map((action, actionIndex) => (
          <Stack alignItems="center" key={`action-bar-${actionIndex}`}>
            <IconButton
              sx={{
                color: 'white',
                backgroundColor: blue['600'],
                width: theme => theme.sizing.width.button.action,
                height: theme => theme.sizing.height.button.action,
                '&:hover': {
                  backgroundColor: blue['400']
                }
              }}
              onClick={action.onClick}
            >
              {action.icon}
            </IconButton>
            <CustomTypography variant="h6" sx={{ color: 'white', mt: 1 }}>
              {action.name}
            </CustomTypography>
          </Stack>
        ))
      }
    </Stack>
  );
}

export default ActionBar;
