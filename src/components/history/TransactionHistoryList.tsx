/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import Stack from '@mui/material/Stack';
import Slide from '@mui/material/Slide';
import { SxProps } from '@mui/material/styles';
import CircularProgress from '@mui/material/CircularProgress';
import CustomTypography from '@components/shared/custom/CustomTypography';
import { TransactionHistoryItemWithRef } from '@components/history/TransactionHistoryItem';

import { History } from '@interfaces/history';
import { mapHistoryToItem } from '@pages/HistoryPage/mapper';

interface TransactionHistoryListProps {
  isLoading: boolean
  histories: History[]
  sx?: SxProps
}

const TransactionHistoryList: React.FC<TransactionHistoryListProps> = ({ isLoading, histories, sx }) => {
  const historyListRef = React.useRef();

  return (
    <Stack px={3} ref={historyListRef} sx={{ ...sx }}>
      <CustomTypography variant='h3' sx={{ mb: 2, color: 'white' }}>
        History
      </CustomTypography>
      {isLoading && (
        <Stack width="100%" alignItems="center">
          <CircularProgress sx={{ color: 'white' }} />
        </Stack>
      )}
      <Stack>
        {
          histories.map((history, historyIndex) => (
            <Slide
              in
              mountOnEnter
              unmountOnExit
              direction="left"
              container={historyListRef.current}
              key={`transaction-history-item-${historyIndex}`}
            >
              <TransactionHistoryItemWithRef
                {...mapHistoryToItem(history)}
                sx={{ mt: historyIndex !== 0 ? 1 : 0 }}
              />
            </Slide>
          ))
        }
      </Stack>
    </Stack>
  );
}

export default TransactionHistoryList;
