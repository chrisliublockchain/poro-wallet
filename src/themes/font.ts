import { createTheme } from '@mui/material/styles';

export default createTheme({
  font: {
    size: {
      h1: '28px',
      h2: '24px',
      h3: '20px',
      h4: '16px',
      h5: '14px',
      h6: '12px',
      p: '10px'
    }
  }
});
