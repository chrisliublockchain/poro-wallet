/* eslint-disable @typescript-eslint/no-misused-promises */
import React from 'react';

import Stack from '@mui/material/Stack';
import LoadingButton from '@mui/lab/LoadingButton';
import { red, blue, grey } from '@mui/material/colors';
import { SxProps, styled } from '@mui/material/styles';

import CustomTypography from './CustomTypography';

interface CustomButtonProps {
  label: string
  onClick: () => void
  sx?: SxProps
  disabled?: boolean
}

const ConfirmLoadingButton = styled(LoadingButton)(({ theme }) => ({
  color: 'white',
  fontWeight: 'bold',
  textTransform: 'unset',
  backgroundColor: blue[600],
  '&:hover': {
    backgroundColor: blue[500]
  },
  '&.Mui-disabled': {
    color: grey[800],
    backgroundColor: grey[600]
  },
  '&.MuiLoadingButton-loading': {
    color: 'transparent'
  },
  '& > .MuiLoadingButton-loadingIndicator': {
    color: 'white'
  }
}));

const CustomButton: React.FC<CustomButtonProps> = ({ label, onClick, sx, disabled = false }) => {
  const [error, setError] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);

  const handleOnClick = async (): Promise<void> => {
    try {
      setIsLoading(true);
      await onClick();
      setIsLoading(false);
    } catch (err: any) {
      setIsLoading(false);
      setError(err.message);
    }
  }

  return (
    <Stack width="100%">
      {
        error.length > 0 && (
          <CustomTypography variant="h5" sx={{ color: red['500'], mb: 1 }}>
            {error}
          </CustomTypography>
        )
      }
      <ConfirmLoadingButton
        fullWidth
        variant="contained"
        loading={isLoading}
        disabled={disabled}
        onClick={handleOnClick}
        sx={{ ...sx }}
      >
        {label}
      </ConfirmLoadingButton>
    </Stack>
  );
}

export default CustomButton;
