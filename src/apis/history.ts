/* eslint-disable import/prefer-default-export */
import axios from 'axios';

import { AddHistoryRequestParams } from '@interfaces/apis/history';

export const addHistory = async (params: AddHistoryRequestParams): Promise<boolean> => {
  try {
    const server = String(process.env.REACT_APP_PORO_WALLET_BACKEND);
    const url = `${server}/v1/history/create`;
    const result = await axios.post(url, params, { headers: { 'Content-Type': 'application/json' } });

    if (result.data.success === true) {
      return true;
    }

    throw Error(result.data.message);
  } catch (error: any) {
    throw Error(`Unexpected error while adding transaction record to database: ${error.message}`);
  }
}
