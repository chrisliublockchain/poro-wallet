import React from 'react';

import Typography from '@mui/material/Typography';
import { SxProps, styled } from '@mui/material/styles';

import ForwardRefWrapper from '../ForwardRefWrapper';

type FontSizeVariant = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p';

interface CustomTypographyProps {
  children: any
  variant: FontSizeVariant
  sx?: SxProps
  assignedRef?: React.ForwardedRef<any>
}

const PreBuildCustomTypography = styled(Typography)(({ variant, theme }) => ({
  lineHeight: 1,
  fontSize: theme.font.size[variant === 'body1' ? 'p' : variant as FontSizeVariant]
}));

const CustomTypography: React.FC<CustomTypographyProps> = ({ variant, children, sx, assignedRef }) => {
  return (
    <PreBuildCustomTypography
      ref={assignedRef}
      variant={variant === 'p' ? 'body1' : variant}
      sx={{ ...sx }}
    >
      {children}
    </PreBuildCustomTypography>
  );
};

export const CustomTypographyWithRef = ForwardRefWrapper(CustomTypography);

export default CustomTypography;
