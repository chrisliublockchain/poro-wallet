import { createTheme } from '@mui/material/styles';

export default createTheme({
  color: {
    background: {
      external: '#52526b',
      internal: '#191b2a'
    },
    border: {
      bar: '#10111a',
      navigationTabContent: '#6c757d',
      selectedNavigationTabContent: '#adb5bd',
      selectedNavigationTabIcon: '#dee2e6'
    }
  }
});
