/* eslint-disable import/prefer-default-export */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Balance } from '@interfaces/balance';
import { BuildTxParams, BuiltTx } from '@interfaces/transaction';

import { mapEvmTxBuilder } from './evm';
import { mapRippleTxBuilder } from './ripple';

export const buildTx = async (asset: Balance, params: BuildTxParams, node: string): Promise<BuiltTx> => {
  switch (asset.blockchain) {
    case 'goerli':
    case 'mumbai':
      return (await mapEvmTxBuilder(asset, params, node));
    case 'ripple-testnet':
      return (await mapRippleTxBuilder(asset, params, node));
    // case 'stellar-testnet':
    //   return {};
    // case 'theta':
    //   return {};
    default:
      throw Error('Unsupported blockchain.');
  }
}
