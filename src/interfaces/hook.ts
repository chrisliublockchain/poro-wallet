import { PendingTransaction } from './redux';

// useFeeLoader
export interface EvmFee {
  maxFeePerGas?: string
  maxPriorityFeePerGas?: string
}

export interface Fee extends EvmFee {
  fee: string
}

export interface FeeLoader {
  isLoading: boolean
  fee?: Fee
}

// usePendingTransactionProcessor
export interface PendingTransactionProcessorState {
  isLoading: boolean
  pending: PendingTransaction[]
}
