/* eslint-disable import/prefer-default-export */
import { PendingTransaction } from '@interfaces/redux';

import { State } from '.';

export const selectPendingTransactions = (state: State): PendingTransaction[] => state.pendingTransactions;
