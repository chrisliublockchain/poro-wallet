/* eslint-disable import/prefer-default-export */
import axios from 'axios';

import { BuiltTx } from '@interfaces/transaction';
import { BlockchainName } from '@interfaces/blockchain';

const getSigningEndpoint = (blockchain: BlockchainName): string => {
  const server = String(process.env.REACT_APP_PORO_WALLET_BACKEND);

  switch (blockchain) {
    case 'goerli':
    case 'mumbai':
      return `${server}/v1/transaction/sign/evm`;
    case 'ripple-testnet':
      return `${server}/v1/transaction/sign/ripple`;
    // case 'stellar-testnet':
    //   return {};
    // case 'theta':
    //   return {};
    default:
      throw Error('Unsupported blockchain.');
  }
}

export const sendTx = async (blockchain: BlockchainName, signedTx: string): Promise<string> => {
  try {
    const server = String(process.env.REACT_APP_PORO_WALLET_BACKEND);
    const url = `${server}/v1/transaction/send`;
    const result = await axios.post(url, {
      blockchain,
      signed: signedTx
    }, { headers: { 'Content-Type': 'application/json' } });

    if (result.data.success === true) {
      return result.data.hash;
    }

    throw Error(result.data.message);
  } catch (error: any) {
    throw Error(`Unexpected error while sending the transaction: ${error.message}.`);
  }
}

export const signTx = async (blockchain: BlockchainName, builtTx: BuiltTx): Promise<string> => {
  try {
    const url = getSigningEndpoint(blockchain);
    const result = await axios.post(url, {
      ...builtTx,
      blockchain
    }, { headers: { 'Content-Type': 'application/json' } });

    if (result.data.success === true) {
      return result.data.signed;
    }

    throw Error(result.data.message);
  } catch (error: any) {
    throw Error(`Unexpected error while signing the transaction: ${error.message}.`);
  }
}
