import { createTheme } from '@mui/material/styles';

export default createTheme({
  sizing: {
    height: {
      avatar: {
        asset: {
          large: '60px',
          normal: '30px'
        },
        action: '16px',
        nativeAsset: {
          large: '28px',
          normal: '14px'
        },
        transactionHistoryType: '36px'
      },
      button: {
        action: '36px',
        backNavigation: '48px'
      },
      icon: {
        backNavigation: '20px',
        bottomNavigationBar: '22px'
      },
      layout: {
        root: '600px',
        rootLogoBar: '36px',
        headerNavigationBar: '48px',
        bottomNavigationBar: '48px'
      }
    },
    width: {
      avatar: {
        asset: {
          large: '60px',
          normal: '30px'
        },
        action: '16px',
        nativeAsset: {
          large: '28px',
          normal: '14px'
        },
        transactionHistoryType: '36px'
      },
      button: {
        action: '36px',
        backNavigation: '48px'
      },
      icon: {
        backNavigation: '20px',
        bottomNavigationBar: '22px'
      },
      layout: {
        root: '330px'
      }
    }
  }
});
