import React from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import SendIcon from '@mui/icons-material/Send';
import ActionBar from '@components/shared/ActionBar';
import DownloadIcon from '@mui/icons-material/Download';
import RootContainer from '@components/layouts/RootContainer';
import CircularProgress from '@mui/material/CircularProgress';
import CustomSelect from '@components/shared/custom/CustomSelect';
import CustomAvatar from '@components/shared/custom/CustomAvatar';
import HeaderNavigationBar from '@components/layouts/HeaderNavigationBar';
import CustomTypography from '@components/shared/custom/CustomTypography';

import { Balance } from '@interfaces/balance';
import { toDisplayBalance } from '@utils/formatting';

const filterOptions = [
  { value: 'all', display: 'All' },
  { value: 'deposit', display: 'Deposit' },
  { value: 'withdrawal', display: 'Withdrawal' }
];

const AssetPage: React.FC = () => {
  const navigate = useNavigate();
  const { state }: { state: { asset: Balance, nativeAsset?: Balance } | null } = useLocation();

  const [filter, setFilter] = React.useState(filterOptions[0].value);

  return (
    <RootContainer>
      <HeaderNavigationBar
        onClick={() => navigate('/')}
        title={state === null ? undefined : state.asset.displayName}
      />
      <Stack alignItems="center" height="100%" width="100%" px={3} boxSizing="border-box">
        {
          state === null
            ? (
                <Stack height="100%" justifyContent="center">
                  <CircularProgress sx={{ color: 'white' }} />
                </Stack>
              )
            : (
                <>
                  <Stack alignItems="center" width="100%" minHeight={200}>
                    <CustomAvatar
                      size='large'
                      asset={{ displayName: state.asset.displayName, icon: state.asset.icon ?? '' }}
                      nativeAsset={
                        typeof state.nativeAsset !== 'undefined'
                          ? { displayName: state.nativeAsset.displayName, icon: state.nativeAsset.icon ?? '' }
                          : undefined
                      }
                    />
                    <CustomTypography variant='h3' sx={{ my: 2, color: 'white' }}>
                      {`${toDisplayBalance(state.asset)} ${state.asset.ticker}`}
                    </CustomTypography>
                    <ActionBar
                      actions={[{
                        name: 'Send',
                        icon: <SendIcon
                          sx={{
                            width: theme => theme.sizing.width.avatar.action,
                            height: theme => theme.sizing.height.avatar.action
                          }}
                        />,
                        onClick: () => ['native', 'token', 'erc-20'].includes(state.asset.standard) &&
                          navigate('/send/token', { state })
                      }, {
                        name: 'Receive',
                        icon: <DownloadIcon
                          sx={{
                            width: theme => theme.sizing.width.avatar.action,
                            height: theme => theme.sizing.height.avatar.action
                          }}
                        />,
                        onClick: () => {}
                      }]}
                      sx={{ width: '100%' }}
                    />
                  </Stack>
                  <Divider sx={{ width: '100%', borderColor: 'white' }} />
                  <Stack alignItems="center" height="100%" width="100%" mt={2}>
                    <Stack direction="row" justifyContent="space-between" alignItems="center" width="100%">
                      <CustomTypography variant='h6' sx={{ color: 'white' }}>
                        Recent Transactions
                      </CustomTypography>
                      <CustomSelect
                        value={filter}
                        setValue={setFilter}
                        options={filterOptions}
                        sx={{
                          width: '120px'
                        }}
                      />
                    </Stack>
                  </Stack>
                </>
              )
        }
      </Stack>
    </RootContainer>
  );
}

export default AssetPage;
