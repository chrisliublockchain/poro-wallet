import { grey } from '@mui/material/colors';
import { createTheme, experimental_sx as sx } from '@mui/material/styles';

import FontTheme from '@themes/font';
import ColorTheme from '@themes/color';
import SizingTheme from '@themes/sizing';

import { IFontTheme, IFontThemeOptions } from '@interfaces/themes/font';
import { IColorTheme, IColorThemeOptions } from '@interfaces/themes/color';
import { ISizingTheme, ISizingThemeOptions } from '@interfaces/themes/sizing';

declare module '@mui/material/styles' {
  interface Theme {
    font: IFontTheme
    color: IColorTheme
    sizing: ISizingTheme
  }
  interface ThemeOptions {
    font?: IFontThemeOptions
    color?: IColorThemeOptions
    sizing?: ISizingThemeOptions
  }
}

export default createTheme(
  {
    components: {
      MuiTab: {
        styleOverrides: {
          root: {
            height: '36px',
            minHeight: '36px'
          }
        }
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            minHeight: '36px'
          }
        }
      },
      MuiButtonBase: {
        defaultProps: {
          disableRipple: true
        }
      },
      MuiPaper: {
        styleOverrides: {
          root: {
            color: 'white',
            backgroundColor: grey['900']
          }
        }
      },
      MuiOutlinedInput: {
        styleOverrides: {
          input: sx({
            py: 1,
            px: '10px'
          }),
          root: {
            backgroundColor: grey['900'],
            borderRadius: '8px',
            input: {
              color: 'white',
              height: '28px',
              fontSize: FontTheme.font.size.h5
            },
            '&:hover fieldset': {
              borderColor: `${grey['400']} !important`
            }
          },
          notchedOutline: {
            borderWidth: '1px !important',
            borderColor: grey['600'],
            '.Mui-focused &': {
              borderColor: `${grey['400']} !important`
            }
          }
        }
      }
    }
  },
  FontTheme,
  ColorTheme,
  SizingTheme
);
