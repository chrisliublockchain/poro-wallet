interface IColorBackground {
  external: string
  internal: string
}

interface IColorBorder {
  bar: string
  navigationTabContent: string
  selectedNavigationTabIcon: string
  selectedNavigationTabContent: string
}

interface IColorBackgroundOptions {
  external?: string
  internal?: string
}

interface IColorBorderOptions {
  bar?: string
  navigationTabContent?: string
  selectedNavigationTabIcon?: string
  selectedNavigationTabContent?: string
}

export interface IColorTheme {
  border: IColorBorder
  background: IColorBackground
}

export interface IColorThemeOptions {
  border?: IColorBorderOptions
  background?: IColorBackgroundOptions
}
