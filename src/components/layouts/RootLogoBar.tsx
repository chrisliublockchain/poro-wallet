import React from 'react';

import Stack from '@mui/material/Stack';
import CustomTypography from '@components/shared/custom/CustomTypography';

const RootLogoBar: React.FC = () => {
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="center"
      sx={{
        backgroundColor: theme => theme.color.border.bar,
        height: theme => theme.sizing.height.layout.rootLogoBar,
        minHeight: theme => theme.sizing.height.layout.rootLogoBar
      }}
    >
      <CustomTypography variant="h5" sx={{ color: 'white' }}>
        PoroWallet
      </CustomTypography>
    </Stack>
  );
};

export default RootLogoBar;
