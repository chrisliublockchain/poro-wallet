/* eslint-disable import/prefer-default-export */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { PendingTransaction } from '@interfaces/redux';

const initialState: PendingTransaction[] = [];

export const pendingTransactionsSlice = createSlice({
  name: 'pendingTransactions',
  initialState,
  reducers: {
    insert: (state, action: PayloadAction<PendingTransaction>) => {
      state.push(action.payload);
    },
    delete: (state, action: PayloadAction<string>) => {
      return state.filter((tx) => tx.hash !== action.payload);
    }
  }
});

export const {
  insert: insertPendingTx,
  delete: deletePendingTx
} = pendingTransactionsSlice.actions;
