import React from 'react';

import Box from '@mui/material/Box';
import { red } from '@mui/material/colors'
import TextField from '@mui/material/TextField';

import CustomTypography from './CustomTypography';

interface CustomInputFieldProps {
  value: string
  label?: string
  postFailValidation?: (input: string) => void
  postSuccessValidation?: (input: string) => void
  validation?: (input: string) => boolean | string
}

const CustomInputField: React.FC<CustomInputFieldProps> = ({ value, label, validation, postFailValidation, postSuccessValidation }) => {
  const [error, setError] = React.useState<string>();

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (typeof validation !== 'undefined' && typeof postFailValidation !== 'undefined' && typeof postSuccessValidation !== 'undefined') {
      const isValid = validation(e.target.value);
      if (typeof isValid === 'boolean' && isValid) {
        setError('');
        postSuccessValidation(e.target.value);
      } else {
        setError(e.target.value.length === 0 ? '' : String(isValid));
        postFailValidation(e.target.value);
      }
    }
  }

  return (
    <Box
      sx={{
        width: '100%',
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      {
        typeof label !== 'undefined' && (
          <CustomTypography variant="h6" sx={{ color: 'white', mb: '4px' }}>
            {label}
          </CustomTypography>
        )
      }
      <TextField
        label=""
        fullWidth
        value={value}
        onChange={handleOnChange}
      />
      {
        typeof error !== 'undefined' && error.length > 0 && (
          <CustomTypography variant="h6" sx={{ color: red['500'], mt: '4px' }}>
            {error}
          </CustomTypography>
        )
      }
    </Box>
  );
}

export default CustomInputField;
