interface FontSize {
  h1: string
  h2: string
  h3: string
  h4: string
  h5: string
  h6: string
  p: string
}

interface FontSizeOptions {
  h1?: string
  h2?: string
  h3?: string
  h4?: string
  h5?: string
  h6?: string
  p?: string
}

export interface IFontTheme {
  size: FontSize
}

export interface IFontThemeOptions {
  size?: FontSizeOptions
}
