import React, { useState, SyntheticEvent } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';

import globalNavigations from '../../navigation';

const NavigationBar: React.FC = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const navigations = globalNavigations.filter((gNav) => typeof gNav.icon !== 'undefined' && typeof gNav.label !== 'undefined');
  const bottomNavigationValue = navigations.find((nav) => nav.path === pathname)?.label?.toLowerCase();

  const [value, setValue] = useState<string>(bottomNavigationValue ?? 'home');

  const onClickNavigationTab = (_: SyntheticEvent<Element, Event>, newValue: any): void => {
    const destination = newValue === 'home' ? '/' : newValue;
    navigate(destination);
    setValue(newValue);
  }

  return (
    <BottomNavigation
      showLabels
      value={value}
      onChange={onClickNavigationTab}
      sx={{
        backgroundColor: theme => theme.color.border.bar,
        height: theme => theme.sizing.height.layout.bottomNavigationBar,
        minHeight: theme => theme.sizing.height.layout.bottomNavigationBar,
        '& .MuiBottomNavigationAction-label': {
          mt: '1px',
          fontSize: '12px'
        },
        '& .MuiSvgIcon-root': {
          width: theme => theme.sizing.width.icon.bottomNavigationBar,
          height: theme => theme.sizing.height.icon.bottomNavigationBar
        },
        '& .Mui-selected': {
          color: theme => `${theme.color.border.selectedNavigationTabContent} !important`,
          '& .MuiBottomNavigationAction-label': {
            mt: '1px',
            fontSize: '12px',
            fontWeight: 'bold'
          },
          '& .MuiSvgIcon-root': {
            color: theme => `${theme.color.border.selectedNavigationTabIcon} !important`
          }
        }
      }}
    >
      {
        navigations.map((navigation, navigationIndex) => (
          <BottomNavigationAction
            icon={navigation.icon}
            label={navigation.label}
            value={String(navigation.label).toLowerCase()}
            key={`bottom-navigation-bar-tab-${navigationIndex}`}
            sx={{
              p: 0,
              color: theme => theme.color.border.navigationTabContent
            }}
          />
        ))
      }
    </BottomNavigation>
  );
};

export default NavigationBar;
